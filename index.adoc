:author: Marc Cristobal
:email: marc7cs@gmail.com // mcristobal@alumnat.copernic.cat
:revdate: 13/03/2024
:revnumber: la versió p.ex. 1.0
:doctype: book
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:icons: font
:imagesdir: ./images

= El meu projecte

image::image.png[align="center"]
image::meme.png[align="center"]

L'objectiu del *projecte* X és la funció Z

== Sprint 1

=== Release Notes

== Sprint 2

=== Release Notes

== Sprint 3

== Sprint 4

== Sprint 5